#ifndef PARAMS_HPP
#define PARAMS_HPP
#include <cmath>
#include <fstream>


/**
   @brief Enumeration of numerical schemes
*/
enum NumScheme {Euler, CrankNicholson, Godunov};

/**
   @brief Enumeration of models
*/
enum Model {Galtier19_9, Galtier19_29};

/**
   @brief Parameters
*/
class params {
public:
  /**
     @brief Initialize parameters from values
     @param ptmax Maximum time for run
     @param pnk Number of grid points for wavenumbers
     @param pkmin Smallest wavenumber
     @param plambda Ratio between successive wavenumbers
  */
  params (double ptmax, int pnk, double pkmin, double plambda) : 
    nk (pnk), tmax (ptmax), kmin (pkmin), lambda (plambda),
    kmax (pkmin * pow (plambda, pnk - 1)), ditout (10), dtout (-1.),
    numscheme (CrankNicholson), nuexp (2), nu (1e-5) {};
  params (const char * filename);
  void write (std::ostream & s);
  double tmax;  /// @brief Maximum time for run
  int nk;       /// @brief Number of grid points for wavenumbers
  double kmin;  /// @brief Smallest wavenumber
  double kmax;  /// @brief Largest wavenumber
  double lambda;/// @brief Ratio between successive wavenumbers
  int ditout;   /// @brief Number of time steps between outputs. If negative use %dtout instead.
  double dtout; /// @brief Time interval between outputs
  NumScheme numscheme; /// @brief Numerical scheme
  double nuexp; /// @brief Exponent for dissipation
  double nu;    /// @brief Dissipation factor
  double lnuexp; /// @brief Exponent for dissipation at large scales
  double lnu;    /// @brief Dissipation factor at large scales
  double H;     /// @brief Universe expansion factor
  Model model;  /// @brief Model equation to simulate
};

#endif
