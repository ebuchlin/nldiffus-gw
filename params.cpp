#include "params.hpp"

#include <iostream> // for debug

/**
   @brief Read parameters from file
   @param filename File name
 */
params::params (const char * filename) {
  std::ifstream f (filename);
  std::string comment;
  std::string strns, strm;
  f >> comment >> tmax
    >> comment >> nk
    >> comment >> kmin
    >> comment >> lambda
    >> comment >> ditout
    >> comment >> dtout
    >> comment >> strns
    >> comment >> nuexp
    >> comment >> nu
    >> comment >> lnuexp
    >> comment >> lnu
    >> comment >> H
    >> comment >> strm;
  kmax = kmin * pow (lambda, nk - 1);
  numscheme = (strns == "Euler" ? Euler : CrankNicholson);
  model = (strm == "Galtier19_9" ? Galtier19_9 : Galtier19_29);
//   while (true) {
//     std::cin >> comment;
//     std::cout << comment;
//   }
  write(std::cout);
}

/**
   @brief Write parameters to output stream
   @param s Output stream
*/
void
params::write (std::ostream & s) {
  s << tmax << " " << nk << " " << kmin << " " << lambda << " "
    << ditout << " " << dtout << " "
    << (numscheme == Euler ? "Euler " : "CrankNicholson ")
    << nuexp << " " << nu << " "
    << lnuexp << " " << lnu << " "
    << H << " "
    << (model == Galtier19_9 ? "Galtier19_9" : "Galtier19_29")
    << std::endl;
}
