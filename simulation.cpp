#include "simulation.hpp"


/**
   @brief Write output for current time step of simulation
*/
void
simulation::write_output () {
  outf << t << std::endl;
  f.write (outf);
  outdfdt << t << std::endl;
  dfdt.write (outdfdt);
  outflux << t << std::endl;
  flux.write (outflux);
  f.compute_ntot (p);
  outntot << f.ntot << std::endl;
  ntout++;
}


/**
   @brief Advance simulation by one time step

   Numerical scheme chosen according to p.numscheme
*/
void
simulation::advance_dt () {
  compute_flux ();
  compute_dfdt ();
  if (istout) write_output ();
  compute_dt ();
  if (!(it % 1000)) print_info (std::cout);

  switch (p.numscheme) {
  case Euler:
    for (int i = 0; i < p.nk; i++) {
      f.nn[i] += dt * dfdt.nn[i];
    }
    break;
  case CrankNicholson:
    for (int i = 0; i < p.nk; i++) {
      f.nn[i] += dt * (1.5 * dfdt.nn[i] - 0.5 * dfdt1.nn[i]);
    }
    break;
  case Godunov:
    // TODO
      
    break;
  }

  // dissipation (at small and large scales)
  for (int i = 0; i < p.nk; i++) {
    f.nn[i] /= (1. + dt * p.nu * pow(k[i], p.nuexp));
    f.nn[i] /= (1. + dt * p.lnu * pow(k[i], p.lnuexp));
  }
  t += dt;
  ++it;
}


/**
   @brief Compute time step

   Use actual computed time derivative of fields (where fields are not
   too small). CFL factor defined here. Updates %istout.
*/
void
simulation::compute_dt () {
  double maxinvdt = 0;
  double invdt;
  for (int i = 2; i < p.nk - 2; i++) {
    if (std::abs (f.nn[i]) > 1e-50) {
      invdt = std::abs (dfdt.nn[i] / f.nn[i]);
      if (invdt > maxinvdt) maxinvdt = invdt;
    }
  }
  dt = .1 / maxinvdt;  // CFL factor is .1

  // Update istout: do we output data at next timestep?
  istout = false;
  if (p.ditout > 0) {
    if ((it + 1) % p.ditout == 0) istout = true;
  } else {
    double ddt = fmod (t + dt, p.dtout);
    if (ddt < fmod (t, p.dtout)) {
      istout = true;
      // temporarily adjust dt
      dt = (dt - ddt) * 1.0000001;
    }
  }
}


/**
   @brief Compute flux of fields (non-linear term only)
*/
void
simulation::compute_flux () {
  if (p.model == Galtier19_29) { // Eq. (29) of Galtier et al. (2019)
    for (int i = 0; i < p.nk; i++) {
      if (i == 0 || i == p.nk - 1) {
        flux.nn[i] = 0;
        continue;
      }
      flux.nn[i] = k[i] * k[i] * f.nn[i] * f.nn[i] *
        (k[i+1] * f.nn[i+1] - k[i-1] * f.nn[i-1]);
      flux.nn[i] /= k[i-1] - k[i+1]; // - sign
    }
  } else if (p.model == Galtier19_9) { // Eq. (9) of Galtier et al. (2019)
    // integral of flux (to be derived to give flux)
    std::vector<double> intQ(p.nk);
    // Compute \partial_k N
    std::vector<double> dndk(p.nk);
    for (int i = 1; i < p.nk - 1; i++)
      dndk[i] = (f.nn[i+1] - f.nn[i-1]) / (k[i+1] - k[i-1]);
    // compute integral of flux intQ
    for (int i = 1; i < p.nk - 1; i++) {
      const double & di = dndk[i];
      const double & ni = f.nn[i];
      const double & ki = k[i];
      double & ifi = intQ[i];
      // Step A
      ifi = 2. * ni - ki * ki * (dndk[i+1] - dndk[i-1]) / (k[i+1] - k[i-1]);
      // Step B
      ifi *= ni;
      // Step C
      ifi += 2. * ki * di * (ki * di - 2. * ni);
      // Step D
      ifi *= ki * ki * ki * ni;
    }
    // Step E (opposite of derivative, cannot be done in-place)
    for (int i = 1; i < p.nk - 1; i++)
      flux.nn[i] = (intQ[i-1] - intQ[i+1]) / (k[i+1] - k[i-1]);
  } else {
    std::cout << "Unimplemented model equation " << p.model << std::endl;
    exit(1);
  }
}


/**
   @brief Compute time derivative of fields
   
   Pre-requisite: flux has been computed.
*/
void
simulation::compute_dfdt () {
  // for numerical schemes that use time derivatives at previous time step
  if (p.numscheme == CrankNicholson) dfdt1 = dfdt;
  
  for (int i = 0; i < p.nk; i++) {
    if (i == 0 || i == p.nk - 1) {
      dfdt.nn[i] = 0;
      continue;
    }
    // non-linear term (using flux)
    dfdt.nn[i] = flux.nn[i+1] - flux.nn[i-1];
    dfdt.nn[i] /= k[i-1] - k[i+1]; // - sign
    // Universe expansion term
    dfdt.nn[i] += p.H * k[i] * (f.nn[i+1] - f.nn[i-1]) / (k[i+1] - k[i-1]);
  }
}


/**
   @brief Print formatted information on simulation
*/
void
simulation::print_info (std::ostream & s) const {
  s << "it: " << it << "   t: " << t << "   dt: " << dt << std::endl;
}


/**
   @brief Run simulation
*/
void
simulation::run () {
  f.init (p);
  {
    std::ofstream fp ("out_params");
    p.write (fp);
    fp.close();
  }
  while (t <= p.tmax && it < 1000000000) {
    advance_dt ();
//     std::cout << "it " << it << "  t " << t << std::endl;
  }
  {
    std::ofstream fn ("out_nt");
    fn << ntout << std::endl;
  }
}
