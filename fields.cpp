#include "fields.hpp"
#include <cmath>

/**
   @brief Initialize fields
   @param p Parameters

   Initialization of fields must be written in this function
 */
void
fields::init (const params & p) {
  int ik0 = p.nk - 50;
  double nmax = 0.;
  bool flatten = false;

  k[ik0] = p.kmin * pow (p.lambda, ik0);  // has to be known first
  for (int ik = 0; ik < nk; ik++) {
    k[ik] = p.kmin * pow (p.lambda, ik);
    if (false) {
      // symmetric power-laws, around k0, max is 1 at k0
      const double a = 3.; // exponent
      if (ik < ik0) nn[ik] = pow (k[ik ] / k[ik0], a);
      else          nn[ik] = pow (k[ik0] / k[ik ], a);
      flatten = true;
    }
    if (true) {
      // asymmetric spectrum around k0
      const double a[] = {3., 2.}; // exponents before and after k0
      nn[ik] = pow (k[ik] / k[ik0], a[0]) * exp (-pow (k[ik] / k[ik0], a[1]));
     }
    if (nn[ik] < 1e-100) nn[ik] = 1e-100;
    if (nn[ik] > nmax) nmax = nn[ik];
  }

  if (flatten) {
    // flatten spectrum from ik0 - 2 to ik0 + 2
    for (int ik = ik0 - 1; ik <= ik0 + 1; ik++) {
      nn[ik] = nn[ik0 - 2];
    }
  }
  
  // normalize by maximum (and some factor)
  for (int ik = 0; ik < nk; ik++) {
    nn[ik] /= nmax * 1e5;
//     std::cout << ik << " " << k[ik] << " "<< nn[ik] << std::endl;
  }
}


/**
   @brief Write fields to output stream
   @param s Output stream
*/
void
fields::write (std::ostream & s) const {
  for (int ik = 0; ik < nk; ik++)
    s << nn[ik] << " ";
  s << std::endl;
}


/**
   @brief Compute total action (update %ntot)
   @param p Simulation parameters
*/
void
fields::compute_ntot (const params & p) {
  ntot = 0.;
  for (int ik = 0; ik < p.nk; ik++)
    ntot += nn[ik] * k[ik];
  ntot *= (p.lambda - 1. / p.lambda) / 2.;
}
