#ifndef FIELDS_HPP
#define FIELDS_HPP
#include <vector>
#include <iostream>
#include "params.hpp"


/**
   @brief  Fields for simulation: + perp, - perp, + par, - par
 */
class fields {
public:
  /**
     @brief Build field from array size
   */
  fields (int n) : nk (n), nn (n), k (n), ntot (0) {};
  /**
     @brief Build fields from parameters
   */  
  fields (params p) : nk (p.nk), nn (p.nk), k (p.nk), ntot (0) {};
  void init (const params & p);
  void write (std::ostream & s) const;
  void compute_ntot (const params & p);
  /**
     @brief Field spectrum N(k)
  */
  std::vector<double> nn; //
  /**
     @brief Wavenumbers k
  */
  std::vector<double> k;
  /**
     @brief Total energy
  */
  double ntot;

private:
  /**
     @brief Size of fields
  */
  int nk;
};


#endif
