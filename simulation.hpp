#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <iostream>
#include <fstream>
#include <iomanip>
#include "fields.hpp"
#include "params.hpp"


/**
   @brief Simulation
*/
class simulation {
public:
  /**
     @brief Initialize simulation from parameters read in file
  */
  simulation () : p ("params.txt"), f (p.nk), dfdt (p.nk), dfdt1 (p.nk), 
                  flux (p.nk),
		  outf ("out_fields"), outdfdt ("out_dfdt"),
		  outntot ("out_ntot"), outflux ("out_flux"),
		  t (0), it (0), k (new double [p.nk]), istout (true),
		  ntout (0) {
    int ik;
    for (ik = 0; ik < p.nk; ik++) k[ik] = p.kmin * pow (p.lambda, ik);
    outf    << std::setprecision(10);
    outdfdt << std::setprecision(10);
    outntot << std::setprecision(10);
    outflux << std::setprecision(10);
  };
  void write_output ();
  void advance_dt ();
  void compute_flux ();
  void compute_dfdt ();
  void compute_dt ();
  void print_info (std::ostream & s) const;
  void run ();
  params p;     /// @brief Simulation parameters
  double * k;   /// @brief Array of wavenumbers
  fields f;     /// @brief Field (wave action)
  fields flux;  /// @brief Flux of field
  fields dfdt;  /// @brief Time derivative of field
  fields dfdt1; /// @brief Time derivative of field at previous time step
  double t;     /// @brief Current time
  double dt;    /// @brief Current time step
  int it;       /// @brief Current time step index
  bool istout;  /// @brief Output data at next step
  int ntout;    /// @brief Number of outputs
  std::ofstream outf;    /// @brief File for output of wave action
  std::ofstream outdfdt; /// @brief File for output of time derivative of wave action
  std::ofstream outntot; /// @brief File for output of total wave action
  std::ofstream outflux; /// @brief File for output of wave action flux
};

#endif
