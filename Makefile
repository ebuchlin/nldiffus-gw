.PHONY: clean doc

CXX=g++
CXXOPTS=-g -O2
LIBS=-lm

all: nldiffus

nldiffus: main.o fields.o params.o simulation.o
	$(CXX) $(CXXOPTS) -o $@ $^ $(LIBS)

%.o: %.cpp
	$(CXX) $(CXXOPTS) -c $<	

doc:
	doxygen

clean:
	rm *.o nldiffus
