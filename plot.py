#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate

from cycler import cycler
# monochrome "color" cycle
#plt.rcParams['axes.prop_cycle'] = cycler('color', ['k']) * cycler('linestyle', ['-', '--', '-.', ':'])
# coloblind-friendly rainbow color cycle
plt.rcParams['axes.prop_cycle'] = cycler('color', ['#781C81','#3F479B','#4277BD','#529DB7','#62AC9B','#86BB6A','#C7B944','#E39C37','#E76D2E','#D92120'])

plt.rc('text', usetex=True)
#plt.rcParams['figure.figsize'] = [4, 3]
plt.rcParams['font.size'] = 16
plt.rc('text.latex', preamble=r'\usepackage{txfonts}')


savefig = True  # save figs, or just show them

tmax, nk, kmin, lam, ditout, dtout, scheme, nuexp, nu, lnuexp, lnu, H, model = np.loadtxt(
    'out_params',
    dtype={
        'names': ('tmax', 'nk', 'kmin', 'lam', 'ditout', 'dtout', 'scheme', 'nuexp', 'nu', 'lnuexp', 'lnu', 'H', 'model'),
        'formats': ('f8', 'i4', 'f8', 'f8', 'i4', 'f8', 'S12', 'f8', 'f8', 'f8', 'f8', 'f8', 'S20')}
    )[()]
model = model.decode()
k = kmin * lam ** range(nk)

nt = np.loadtxt('out_nt', dtype='i4')[()]

def readField(fn, t=None):
    # file reader for fields (wave action, flux, wave action derivative)
    if t is None:
        t = np.zeros(nt)
        readTime = True
    else:
        readTime = False
    ff = np.zeros((nt, nk))
    with open(fn) as f:
        for i in range(nt):
            tmp = np.fromstring(f.readline(), sep=' ')[0]
            if readTime: t[i] = tmp
            else: assert t[i] == tmp
            ff[i, :] = np.fromstring(f.readline(), sep=' ')
    if readTime: return ff, t
    else: return ff

fnn, t = readField('out_fields')
dfnndt = readField('out_dfdt', t)
fluxnn = readField('out_flux', t)

# spectra
imin = 0
imax = nt
istep = 12
plt.loglog(k, fnn[imin:imax:istep, :].T, linewidth=1)
plt.xlabel('$k$')
plt.ylabel('$N(k)$')
plt.ylim(1e-12, 1e10)
ks = np.array([1e2, 1e22])
plt.loglog(ks, ks ** (-2./3.) * 1e10, 'k')
plt.text(1e6, 1e6, '-2/3')
if savefig:
    plt.savefig('spec.pdf')
    plt.clf()
else:
    plt.show()

# compensated spectra
plt.loglog(k, fnn[imin:imax:istep, :].T * k[:, np.newaxis]**(2/3), linewidth=1)
plt.xlabel('$k$')
plt.ylabel('$k^{2/3} N(k)$')
plt.ylim(1e1, 1e11)
ks = np.array([1e2, 1e22])
plt.loglog(ks, ks ** 0 * 4e9, 'k')
if savefig:
    plt.savefig('spec-23.pdf')
    plt.clf()
else:
    plt.show()

# compensated spectra
plt.loglog(k, fnn[imin:imax:istep, :].T * k[:, np.newaxis]**(0.6517), linewidth=1)
plt.xlabel('$k$')
plt.ylabel('$k^{0.6517} N(k)$')
plt.ylim(1e1, 1e11)
if savefig:
    plt.savefig('spec-06517.pdf')
    plt.clf()
else:
    plt.show()

# zoom on compensated spectra, and slopes
plt.loglog(k, fnn[imin:imax:istep, :].T * k[:, np.newaxis]**(2/3), linewidth=1)
x=np.array([1e1, 1e7])
plt.loglog(x, 2.5e9 * (x[:, np.newaxis] / x[0]) ** np.array([2/3 - 0.6517]), color='k', ls='solid')
plt.xlabel('$k$')
plt.ylabel('$k^{2/3} N(k)$')
plt.ylim(1e9, 4e9)
if savefig:
    plt.savefig('spec-23z.pdf')
    plt.clf()
else:
    plt.show()

# zoom on compensated spectra, and slopes
plt.loglog(k, fnn[imin:imax:istep, :].T * k[:, np.newaxis]**(0.6517), linewidth=1)
x=np.array([1e1, 1e7])
plt.loglog(x, 2.2e9 * (x[:, np.newaxis] / x[0]) ** np.array([0.6517 - 2/3]), color='k', ls='solid')
plt.xlabel('$k$')
plt.ylabel('$k^{0.6517} N(k)$')
plt.ylim(8e8, 3e9)
plt.gcf().subplots_adjust(left=0.16)
if savefig:
    plt.savefig('spec-06517z.pdf')
    plt.clf()
else:
    plt.show()

# flux
plt.semilogx(k, fluxnn[imin:imax:istep, :].T, linewidth=1)
plt.xlabel('$k$')
plt.ylabel('$Q(k)$')
plt.ylim(-1.5e28, .5e28)
plt.gcf().subplots_adjust(left=0.16) # just to be consistent with zoom on compensated spectra
if savefig:
    plt.savefig('flux.pdf')
    plt.clf()
else:
    plt.show()


# timescales
# non-linear (+ Universe expansion if non-zero)
if H > 0:
    plt.loglog(k, np.abs(fnn[0, :] / dfnndt[0, :]).T, label='$\\tau_{NL+H}$')
else:
    plt.loglog(k, np.abs(fnn[0, :] / dfnndt[0, :]).T, label='$\\tau_{NL}$')
if imax - imin > istep:
    plt.loglog(k, np.abs(fnn[istep:imax:istep, :] / dfnndt[istep:imax:istep, :]).T, linewidth=1)
# viscosities
if nu > 0:
    plt.loglog(k, 1. / (nu * k ** nuexp), color='k', ls='solid', label='$\\tau_{\\nu}$')
if lnu > 0:
    plt.loglog(k, 1. / (lnu * k ** lnuexp), color='k', ls='dashed')
# Universe expansion
if H > 0:
    plt.loglog(k, np.ones_like(k)/H, color='r', ls='solid', label='$\\tau_{\mathrm{H}}$')
plt.legend()
plt.xlabel('$k$')
plt.ylabel('$\\tau$')
plt.ylim(1e-22, 1e0)
if savefig:
    plt.savefig('tau.pdf')
    plt.clf()
else:
    plt.show()

# kmin(t): first wavenumber for which compensated spectrum is higher than some value
def get_kmin(s): # s: one spectrum
    w = np.where(s * k**(0.6517) > 1.4e9) # Threshold
    if len(w) == 0: return np.nan
    else: return k[min(w[0])]

kmin = np.apply_along_axis(get_kmin, 1, fnn)
ax = plt.subplot(1, 1, 1)
tstar = 1.002168e-11
tau = tstar - t
tau[tau <= 0] = np.nan
ax.loglog(tau, kmin, '.-', label='$k_f$')
ax.loglog(tau, 1e59 * tau ** 3.296, color='k', ls='dashed', label='$\\textrm{Slope }3.296$')
ax.set_xlabel('$t_*-t$')
ax.set_ylabel('$k_f$')
ax.legend(loc='lower right')
plt.tight_layout()
ax = plt.axes([.21, .6, .32, .32])
ax.semilogy(t  * 1e11, kmin, '.-')
ax.set_xlabel('$t \\times 10^{11}$')
#ax.set_ylabel('$k_f\,(t)$')
if savefig:
    plt.savefig('kmin.pdf')
    plt.clf()
else:
    plt.show()


kmin = np.apply_along_axis(get_kmin, 1, fnn)
ax = plt.subplot(1, 1, 1)
for tstar in [1.002167e-11, 1.002168e-11, 1.002169e-11]:
    tau = tstar - t
    tau[tau <= 0] = np.nan
    ax.loglog(tau, kmin, '.-', label='$k_f, t_*={}$'.format(tstar))
ax.loglog(tau, 1e59 * tau ** 3.296, color='k', ls='dashed', label='$\\textrm{Slope }3.296$')
ax.set_xlabel('$t_*-t$')
ax.set_ylabel('$k_f$')
ax.legend(loc='lower right')
plt.tight_layout()
ax = plt.axes([.21, .6, .32, .32])
ax.semilogy(t  * 1e11, kmin, '.-', color='k')
ax.set_xlabel('$t \\times 10^{11}$')
#ax.set_ylabel('$k_f\,(t)$')
if savefig:
    plt.savefig('kmin-tstars.pdf')
    plt.clf()
else:
    plt.show()


plt.semilogy(t * 1e11, kmin, '.-', color='k')
plt.xlabel('$t \\times 10^{11}$')
plt.ylabel('$k_f$')
plt.xlim(1.00215, 1.00221)
plt.ylim(1, 1e6)
if savefig:
    plt.savefig('kmin-zoom.pdf')
    plt.clf()
else:
    plt.show()


# check total action conservation
ntot = np.apply_along_axis(scipy.integrate.simps, 1, fnn, k)
plt.plot(t, ntot)
plt.xlabel('$t$')
plt.ylabel('$\\int N(k) dk$')
if savefig:
    plt.savefig('ntot.pdf')
    plt.clf()
else:
    plt.show()
