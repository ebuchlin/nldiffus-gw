#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Check self-similarity of wave action spectrum
find best self-similarity exponents'''

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import widgets

tmax, nk, kmin, lam, ditout, dtout, scheme, nuexp, nu, lnuexp, lnu, H, model = np.loadtxt(
    'out_params',
    dtype={
        'names': ('tmax', 'nk', 'kmin', 'lam', 'ditout', 'dtout', 'scheme', 'nuexp', 'nu', 'lnuexp', 'lnu', 'H', 'model'),
        'formats': ('f8', 'i4', 'f8', 'f8', 'i4', 'f8', 'S12', 'f8', 'f8', 'f8', 'f8', 'f8', 'S20')}
    )[()]
model = model.decode()
k = kmin * lam ** range(nk)

nt = np.loadtxt('out_nt', dtype='i4')[()]

def readField(fn, t=None):
    # file reader for fields (wave action, flux, wave action derivative)
    if t is None:
        t = np.zeros(nt)
        readTime = True
    else:
        readTime = False
    ff = np.zeros((nt, nk))
    with open(fn) as f:
        for i in range(nt):
            tmp = np.fromstring(f.readline(), sep=' ')[0]
            if readTime: t[i] = tmp
            else: assert t[i] == tmp
            ff[i, :] = np.fromstring(f.readline(), sep=' ')
    if readTime: return ff, t
    else: return ff

fnn, t = readField('out_fields')
dfnndt = readField('out_dfdt', t)
fluxnn = readField('out_flux', t)


αs = np.linspace(2, 2.1, num=1)
βs = np.linspace(2.8, 3.2, num=3)
tstars = np.linspace(11.1, 11.2, num=1)

# wave action field
imin = nt // 2
imax = nt
istep = 1
N = fnn[imin:imax:istep, :].T
t = t[imin:imax:istep]


#for α in αs:
    #for β in βs:
        #for tstar in tstars:
            #τ = tstar - t
            #τ[τ < 0] = np.nan
            ## self-similar transform
            ## N(k,τ)τ^α = N₀(k/τ^β, τ)
            ## k' = k / τ^β
            ## N(κ'τ^β, τ)τ^α = N₀(k', τ)
            #plt.loglog(k[:, np.newaxis] / τ[np.newaxis, :]**β, N * τ[np.newaxis, :]**α)
        #plt.ylim(1e-30, 1e0)
        #plt.xlabel('$k\'$')
        #plt.ylabel('$N_0$')
        #plt.show()


fig = plt.figure()
ax = fig.add_subplot(111)
# Adjust the subplots region to leave some space for the sliders and buttons
fig.subplots_adjust(bottom=0.3, top=0.95)

α0 = 2
β0 = 3
tstar0 = 11.065

# Draw the initial plot
# The 'line' variable is used for modifying the line later
τ = tstar0 - t
τ[τ <= 0] = np.nan
lines = ax.loglog(k[:, np.newaxis] / τ[np.newaxis, :]**β0, N * τ[np.newaxis, :]**α0)
ax.set_xlim([1e-5, 1e20])
ax.set_ylim([1e-20, 1e1])
ax.set_xlabel('$k\'$')
ax.set_ylabel('$N_0$')

# Add sliders for tweaking the parameters

# Define an axes area and draw a slider in it
α_slider_ax = fig.add_axes([0.12, 0.2, 0.78, 0.03])
α_slider = widgets.Slider(α_slider_ax, 'α', 1., 3., valinit=α0)
# Same for β
β_slider_ax = fig.add_axes([0.12, 0.15, 0.78, 0.03])
β_slider = widgets.Slider(β_slider_ax, 'β', 2., 5., valinit=β0)
# Same for tstar
tstar_slider_ax = fig.add_axes([0.12, 0.1, 0.78, 0.03])
tstar_slider = widgets.Slider(tstar_slider_ax, '$t^*$', 10., 12., valinit=tstar0)

# Define an action for modifying the line when any slider's value changes
def sliders_on_changed(val):
    for it in range(t.size):
        τ = tstar_slider.val - t[it]
        if τ <= 0:
            lines[it].set_xdata([np.nan])
            lines[it].set_ydata([np.nan])
        else:
            lines[it].set_xdata(k / τ**β_slider.val)
            lines[it].set_ydata(N[:, it] * τ**α_slider.val)
    fig.canvas.draw_idle()

α_slider.on_changed(sliders_on_changed)
β_slider.on_changed(sliders_on_changed)
tstar_slider.on_changed(sliders_on_changed)

# Add a button for resetting the parameters
reset_button_ax = fig.add_axes([0.8, 0.025, 0.1, 0.04])
reset_button = widgets.Button(reset_button_ax, 'Reset', color='1', hovercolor='0.975')
def reset_button_on_clicked(mouse_event):
    α_slider.reset()
    β_slider.reset()
    tstar_slider.reset()
reset_button.on_clicked(reset_button_on_clicked)

plt.show()

# optimization of α given β and t*
β = 3
tstar = 11.065
kp = 1e7
α = np.linspace(1.9, 2.1, 11)
τ = tstar0 - t
τ[τ <= 0] = np.nan
for it in range(t.size):
    plt.semilogy(α, np.interp(kp * τ[it]**β, k, N[:, it]) * τ[it]**α)
plt.xlabel('$\\alpha$')
plt.ylabel('$N_0(k\'_0, t)$')
plt.show()

