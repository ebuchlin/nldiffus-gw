# Nonlinear diffusion modelling of gravitational waves turbulence

## What the code does

This code simulates the evolution of the gravitational wave action spectrum, following a second-order diffusion model.

Reference: Galtier, S., Nazarenko, S., Buchlin, É., and Thalabard, S.
*Nonlinear Diffusion Models for Gravitational Wave Turbulence*.
*Physica D*, **390**, 84, 2019 ([preprint](https://arxiv.org/abs/1809.07623), [DOI](http://dx.doi.org/10.1016/j.physd.2019.01.007)).

The version of the code used to produce the results of this paper is tagged as "Galtier2019" in this git repository.


## How to use the code

### Parameters

Please edit the `params.txt` file. This file contains value of the parameters, preceded by a comment on a separate line. Please do no insert spaces in the comment lines.
* `tmax`: end time of the simulation
* `nk`: number of points on the wavenumber axis
* `kmin`: minimum wavenumber
* `lambda`: ratio between successive values on the wavenumber axis. 1.0717735=2**0.1 corresponds to 10 points per octave.
* `ditout`: number of time steps between outputs. Ignored if negative, then `dtout` should be used.
* `dtout`: time interval between outputs. Ignored if negative, then `ditout` should be used.
* `numscheme`: numerical schemes. Possible values include Euler and CrankNicholson.
* `nuexp`, `nu`: exponent and value for the (hyper)viscous term.
* `lnuexp`, `lnu`: exponent and value for the hypoviscous term.
* `H`: inverse Hubble time (Universe expansion)
* `model`: `Draft14` corresponds to Eq. (23) of the reference paper. `Draft6` is for the 4th order diffusion model (Eq. 6 of the reference paper).

After changing the parameters, you might also want to change the initial spectrum in function `fields::init()` (file `fields.cpp`).

As the inverse cascade is explosive and occurs in a finite time t<sub>*</sub>, you can do a first run with a long `tmax`, see get a sense of t<sub>*</sub> from the simulation log (displayed in the terminal), i.e. see when the time scales become very small, and change `tmax` accordingly.


### Compiling

Run `make`.

You can also delete the compiled files with `make clean`, and `make doc` generates documentation in the `doc/` subdirectory.

### Running

Run `./nldiffus`.

### Plotting results

Plots are produced with Python3.

`plot.py` produces most useful plots at once. Some plot parameters can be adjusted:
* `savefig`: save all plots as PDF files, or show each of them interactively. If `saveFig` is `True`, no plot will be shown on screen, they will only be saved in PDF files in the current directory.
* `imin`, `imax`, and `istep`: minimum, maximum, and step for plotting curves, given some output of the code. For example, if the code outputs results every 0.1 time units (`dtout`=0.1) and `istep` is 10, the curves will be separated by 1 time unit.
* the threshold in the compensated spectra used to define `kmin(t)` (k<sub>f</sub> in the paper) can also be adjusted.

`selfsimilar.py`: interactive demonstration of the spectra self-similarity.


## Licence

The code is distributed as [Free Software](http://www.fsf.org/about/what-is-free-software) under the GNU [GPL](https://www.gnu.org/licenses/gpl.html)v3.

Please cite the reference paper if using it.
Known other papers using derived versions of this code (please contact the authors for their version of the code):
* Nazarenko, S. V.; Grebenev, V. N.; Medvedev, S. B.; Galtier, S. (2019). The focusing problem for the Leith model of turbulence: a self-similar solution of the third kind. *J. Phys. A: Math. Theor.*, **52**, 155501. [DOI](http://dx.doi.org/10.1088/1751-8121/ab0da5)
* David, V.; Galtier, S. (2019). k\_perp^8/3 Spectrum in Kinetic Alfvén Wave Turbulence: Implications for the Solar Wind. *Astrophys. J. Lett.*, **880**, L10. [DOI](http://dx.doi.org/10.3847/2041-8213/ab2fe6)
* Miloshevich, G.; Passot, T.; Sulem, P. L. (2020). Modeling Imbalanced Collisionless Alfvén Wave Turbulence with Nonlinear Diffusion Equations. *Astrophys. J. Lett.*, **888**, L7. [DOI](http://dx.doi.org/10.3847/2041-8213/ab60b1)

A previous version of the same code was used for:
* Galtier, S.; Buchlin, É. (2010). Nonlinear Diffusion Equations for Anisotropic Magnetohydrodynamic Turbulence with Cross-helicity. *Astrophys. J.*, **722**, 1977.[DOI](http://dx.doi.org/10.1088/0004-637X/722/2/1977)
